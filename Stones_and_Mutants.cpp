#include <stdio.h>
#include <iostream>





int main(){
	int n = 0;
	int max = 1;
	int avg = 100000;
	scanf("%i", &n);	

	int *weight = new int [n];

	for (int i = 0; i < n; i++){
		scanf("%i", &weight[i]);
		max *= 2;
	}

	for (int i = 1, temp = 0, current_case = i; i < max; current_case = ++i, temp = 0){
		for (int j = 0; j < n; j++, current_case /= 2){
			if (current_case & 1){
				temp += weight[j];
			}
			else{
				temp -= weight[j];
			}
		}
		temp < 0 ? temp = -temp : 1;
		if (temp < avg){
			avg = temp;
		}
	}

	printf("%i", avg);
	return 0;
}

void merge_sort(long int *mas, int left, int right){
	if (left < right){
		int middle = (left + right) / 2;
		merge_sort(mas, left, middle);
		merge_sort(mas, middle+1, right);
		//merge
		long int *tmp_arr = new long int[right - left + 1];
		int i = left;
		int j = middle + 1;
		int cursor = 0;
		while (i <= middle && j <= right){
			if (mas[i] > mas[j]){
				tmp_arr[cursor++] = mas[j];
				j++;
			}
			else{
				tmp_arr[cursor++] = mas[i];
				i++;
			}
		}
		if (i > middle){
			while (j <= right){
				tmp_arr[cursor++] = mas[j];
				j++;
			}
		}
		else if (j > right){
			while (i <= middle){
				tmp_arr[cursor++] = mas[i];
				i++;
			}
		}
		cursor = 0;
		for (int k = left; k <= right; k++){
			mas[k] = tmp_arr[cursor++];
		}
		delete[] tmp_arr;
	}
}

int binary_search(int search_value, long int *mas, int size){
	int average_index = 0, // ���������� ��� �������� ������� �������� �������� �������
		first_index = 0, // ������ ������� �������� � �������
		last_index = size - 1; // ������ ���������� �������� � �������
		//--------------------------------------------------------
	//--------------------------------------------------------

	while (first_index < last_index)
	{
		average_index = first_index + (last_index - first_index) / 2;
		search_value <= mas[average_index] ? last_index = average_index : first_index = average_index + 1; 
	}
	if (mas[last_index] == search_value)
		return last_index;
	else
		return -1;

}


//int main(){
//	int N;
//	scanf("%i", &N);
//	long int *n = new long int[N];
//	for (int i = 0; i < N; i++){
//		scanf("%li", &n[i]);
//	}
//
//	int M;
//	scanf("%i", &M);
//
//	long int *m = new long int[M];
//	long int *m_copy = new long int[M];
//	int *result = new int[M];
//
//	for (int i = 0; i < M; i++){
//		scanf("%li", &m[i]);
//		m_copy[i] = m[i];
//		result[i] = 0;
//	}
//
//
//
//	merge_sort(n, 0, N - 1);
//	merge_sort(m_copy, 0, M - 1);
//
//	int counter = 0;
//	for (int i = 0; i < N - 1; i++){
//		counter++;
//		if (n[i] != n[i + 1]){
//			int pos = binary_search(n[i], m_copy, M);
//			if (pos != -1){
//				result[pos] = counter;
//			}
//			counter = 0;
//		}
//	}
//
//	if (counter == 0){
//		int pos = binary_search(n[N - 1], m_copy, M);
//		if (pos != -1){
//			result[pos] = 1;
//		}
//	}
//	else if(counter > 0){
//		counter++;
//		int pos = binary_search(n[N - 1], m_copy, M);
//		if (pos != -1){
//			result[pos] = counter;
//		}
//	}
//
//	for (int i = 0; i < M; i++){
//		int pos = binary_search(m[i], m_copy, M);
//		printf("%i ", result[pos]);
//	}
//
//
//
//
//	delete[] m_copy;
//	delete[] result;
//	delete[] n;
//	delete[] m;
//	return 0;
//}

