#include <stdio.h>
//#include <iostream>


int right = 0, left = 0;

struct string{
	char str[41];
};


bool mas_check(int size, int value, bool plus, int _add){
	if (_add == 1){
		if (plus){
			if (value + _add < size*size && (value + 1) % size != 0){
				return 1;
			}
		}
		else{
			if (value - _add >= 0 && value % size != 0){
				return 1;
			}
		}
	}
	else{
		if (plus){
			if (value + _add < size*size){
				return 1;
			}
		}
		else{
			if (value - _add >= 0){
				return 1;
			}
		}
	}
	return 0;
}


int main(){
	int n;
	int start, end;
	scanf("%i", &n);
	string *in_str = new string[n];
	int *mas = new int[n*n];
	int *p_mas = new int[n*n];
	int *que = new int[n*n];
	int *dist_mas = new int[n*n];

	char a[41];
	gets(a);
	for (int i = 0; i < n; i++){		//fill strings
		gets(in_str[i].str);
		for (int j = 0; j < n; j++){
			int pos = i*n + j;
			switch (in_str[i].str[j]){
			case '.':
				mas[pos] = 0;
				break;
			case 'X':
				mas[pos] = 0;
				end = pos;
				break;
			case '@':
				mas[pos] = 0;
				start = pos;
				break;
			default:
				mas[pos] = -1;
				break;
			}
		}
	}

	int cur = start;
	que[left] = cur;
	int level = 1;
	for (int i = 0; i < n*n; i++){
		dist_mas[i] = 0;
		p_mas[i] = -1;
	}
	int dist = 1;
	int counter = 0;
	mas[cur] = 1;

	while (cur != end){
		dist--;
		dist_mas[cur] = level;
		if (mas_check(n, cur, 1, 1)){
			int pos = cur + 1;
			if (mas[pos] == 0){
				que[++right] = pos;
				mas[pos] = 1;
				p_mas[pos] = cur;
				counter++;
			}
		}
		if (mas_check(n, cur, 0, 1)){
			int pos = cur - 1;
			if (mas[pos] == 0){
				que[++right] = pos;
				mas[pos] = 1;
				p_mas[pos] = cur;
				counter++;
			}
		}
		if (mas_check(n, cur, 1, n)){
			int pos = cur + n;
			if (mas[pos] == 0){
				que[++right] = pos;
				mas[pos] = 1;
				p_mas[pos] = cur;
				counter++;
			}
		}
		if (mas_check(n, cur, 0, n)){
			int pos = cur - n;
			if (mas[pos] == 0){
				que[++right] = pos;
				mas[pos] = 1;
				p_mas[pos] = cur;
				counter++;
			}
		}


		if (left == right){
			break;
		}

		if (dist == 0){
			dist = counter;
			counter = 0;
			level++;
		}

		left++;
		cur = que[left];

	}
	dist_mas[cur] = level;

	if (p_mas[end] >= 0){
		printf("Y\n");
		for (int i = level; i > 1; i--){
			for (int k = 0; k < n; k++){
				for (int j = 0; j < n; j++){
					if (k*n + j == end){
						in_str[k].str[j] = '+';
						break;
					}
				}
			}
			end = p_mas[end];
		}

		for (int i = 0; i < n; i++){
			puts(in_str[i].str);
		}
	}
	else{
		printf("N");
	}




	delete[] dist_mas;
	delete[] in_str;
	delete[] mas;
	delete[] p_mas;
	delete[] que;
	return 0;
}