#include <stdio.h>
#include <iostream>

bool all_nodes[50];

int counter = 0;


void DFS(int cur_pos, int **matrix, int size, int dest, int D){
	all_nodes[cur_pos] = true;
	if (D > 0){
		for (int i = 0; i < size; i++){
			if (matrix[cur_pos][i] == 1 && all_nodes[i] == 0){
				if (i == dest){
					counter++;
				}
				else{
					DFS(i, matrix, size, dest, D - 1);
				}
			}
		}
	}
	all_nodes[cur_pos] = false;
}

int main(){
	int n, k, a, b, d;

	scanf("%i %i %i %i %i", &n, &k, &a, &b, &d);
	for (int i = 0; i < n; i++){
		all_nodes[i] = 0;
	}
	int *res_routs = new int[d];
	int **mount_matrix = new int *[n];
	int *used_hostels = new int[n];
	for (int i = 0; i < n; i++){
		mount_matrix[i] = new int[n];
		for (int j = 0; j < n; j++){
			mount_matrix[i][j] = 0;
		}
	}

	int temp_i = 0, temp_j = 0;

	for (int i = 0; i < k; i++){
		scanf("%i %i", &temp_i, &temp_j);
		mount_matrix[temp_i - 1][temp_j - 1] = 1;
	}
	a--;
	b--;

	DFS(a, mount_matrix, n, b, d);


	printf("%i", counter);



	//memory release
	for (int i = 0; i < n; i++){
		delete[] mount_matrix[i];
	}
	delete[] mount_matrix;
	return 0;
}